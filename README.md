# ipparts
Background: According to International Accounting Standards, Intellectual
Property may not be recorded on balance sheets. It is partly the cause for
differences between market capitalization of shares and balance sheet asset
values. In my view the origin of the accounting standards relates much to
philosophical and religious convictions whereby "the Creator who forms" new
ideas, "is" sacrificed. The capital of new ideas has been and currently is,
subject to the use of "intelligence". Consequentially it is very difficult
for creative people to monetize their ideas. Ideas are common property and
free to appropriate. Imparting of ideas is protected in secular Constitutions
and Labour Law. During the Enlightenment philosophers motivated rights to
salaries and wages, but they did not motivate rights to ideas generated.
Generating ideas is the first step of improvement. If people who have easy
access to money also have easy access to new ideas, they can easily employ
the necessary labour and buy necessary natural resources to develop valuable
new ideas, whilst excluding the ones who generated the ideas. Refer to the Wiki
for more background information: https://github.com/mdpienaar/ipparts/wiki |||||
Proposal: The proposal herewith is to form an exchange exclusively for
Intellectual Property Parts (ipparts). According to the plan creative people
who need finance to develop their ideas will sell ipparts in their goodwill, copyrights,
and other intellectual property, which may not be capitalized on balance sheets.
Ipparts will be traded as ERC20 Ethereum tokens or as other tokens on another
blockchain. Need help at
https://runkit.com/mdpienaar/ipparts if it is the right place to start the Ipparts Exchange.
Can pay with Africahead Ipparts (AFA) Erc20 token. Forming new Ipparts
and the exchange will be technology available for free (mahala), to anyone
who wants to monetize their assets i.e. copyrights, goodwill, trademarks etc,
which may not be recorded on balance sheets according to International Accounting
Standards. ||||| Currently Africahead is busy with an over the counter ICO at
https://www.africahead.co.za/Africahead/RaisingCapital.html. The website is very 
basic and most is done manually. The purpose of the ICO is raising funds for expansion
of the Africahead business, which will include a new website. After the ICO raised new
capital I want to list the issued Africahead Ipparts (AFA) 
(https://etherscan.io/token/0xfb48e0dea837f9438309a7e9f0cfe7ee3353a84e) on the envisaged
Ipparts Exchange, as the first token, backed by a brand. Then want to see other tokens,
using the idea on the same exchange, which should be exclusively an Ipparts Exchange.
Contact me (Marquard Pienaar) at africahead2@gmail.com.
